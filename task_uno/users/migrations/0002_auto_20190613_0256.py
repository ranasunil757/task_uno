# Generated by Django 2.2.2 on 2019-06-12 21:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='is_active',
            field=models.BooleanField(default=False, help_text='Designates whether this user should be treated as active. select this instead of deleting accounts.', verbose_name='active'),
        ),
    ]
